# Magic Clone Script
Magicbrick-clone-script
This script is a clone of Magic Bricks which offers a wide range of feature with multiple modules as stated wherein each module has different features to perform the intended jobs as designed. The script will be the one stop solution if you are looking to develop a real estate website to enable your presence online. Our Magic Brick Clone Script has all the relevant features and benefits that could result in bringing a hike to your business career. This is Just a demo , we strive to add extra features on regular basis. Please feel free to contact us for more information.
Modules

Admin
Super Admin
Broker / Agent
Property Holders
User

Module Short Description

Super admin is the ultimate user of the application who can create multiple admins as required.
Admin has specific roles such as Accountant who has specific roles to perform as defined by the Super Admin
Broker/Agent can have access to upload the existing properties which are for sale/rent
Property Holder can upload the properties they hold for either sale or rent, where they interact with the users directly and hence the intermediate brokerage can be avoided
User is the person whois looking out for renting or purchasing the properties they are interested in
User Module Features

Creation of account for each user
Easy listing of the properties using images, albums ,etc
Search feature which lets the user to search the site using different criteria like property Square Feet, Location, Area, Price Range, Property Type or using any specific keyword
Update the details like Price or any new conditions which has changed since uploaded
CRUD - Create, Read, Update and Delete the property details
Google Map features or SiteMap can be included to access the property location accurately
SEO features

On Page SEO such as keyword descriptions,titles,image alt tag
SEO friendly URLs
Meta tag, which gives you information about tags
Meta tag, Title, Description & Keywords
Content using unique text to stand out
Google Maps / Sitemap
Integrate Google analytics
Back end features

Admin Panel which lets you perform Admin activities such as user creation,deletion, update,etc
Add / Update / Delete Property details
Easy to use Admin panels with zero expertise on any technical skills
Advertisements which are customized for a property or user can be published from Administration
Added revenue from features such as Google Ads which is enabled by default
Manage Registered users within the site
Manage different Property types such as Villa , Studio, Apartments, Vacation Villas,etc
Define Membership plans for different users such as Agent or users depending on tenure or cost of property
Admin Features

Addition of User Signup
Membership plans and Packages for User / Agent
Manage Member Profile
Manage Property listings
Managed Advanced Property Search
Property Directory
Google Ads and Site Map Management
Google Maps Management
Check Out  Our Product in:


https://www.doditsolutions.com/realestate-script/
http://scriptstore.in/product/99acres-clone-script/
http://phpreadymadescripts.com/

